Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
		The logger is printing only INFO to the console and the log file contains INFO and FINER. This is due to the logger.properties where the ConsoleHandler level only accepts INFO logs while the FileHandler accepts ALL logs.
2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
		This line comes from the JUnit library, when a test case is disabled JUnit writes that to our Logger. The Logger class has the singleton design pattern which means that only one instance of this class exists and JUnit writes to that same object.
3.  What does Assertions.assertThrows do?
		It asserts that the function that we send it will throw an exception of the specified type. If the function executed throws a different exception the assertThrows fails. For example, Assertions.assertThrows(TimerException.class, () -> {Timer.timeMe(-1);}); This will assert if the lambda function throws a TimerException it will be true.
4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
			The serialVersionUID is just an ID that associates with the serialization of a class. We need it to verify
			that a class is the same class when deserialization.
    2.  Why do we need to override constructors?
			To actually create a TimerException class we need basic constructors to do so. We override the constructors
			of the TimerException class so that the Parent of TimerException can also have the same data.
    3.  Why we did not override other Exception methods?
			We don't override them because we don't need other functionality for the Exception. In the TimerException we
			had to override the constructor to create the TimerException that we need.
5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
		The Timer.java has a static block to execute functions before runtime. We need to start a time object that loads the logger.properties and then creates the logger. We want this code to only be executed once and all of our variables to be instantiated so that the rest of the program can run with the required setup done.
6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
		The README.md is a markdown file that styles a README based on user specifications. It is significant because
		sites like bitbucket and github know how to parse .md files. The README is also important because it tells
		a user relevant information about how to use a repository.
7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
		The test was failing because the exception was thrown before the timeNow was set. One the exception was thrown
		it exited the try block and was unable to handle the catch because we are only catching a InterruptedException,
		so we move to the finally block. In the finally block try to perform math on a null object.
8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
		The issue is that we never actually caught the TimerException which should have set the timeNow var to some
		non-null value. So when we get to the finally we can't user the timeToWait variable because it is null and you
		can't a null long to another long thus causing a NullPointerException.
9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel)
10.  Make a printScreen of your eclipse Maven test run, with console
11.  What category of Exceptions is TimerException and what is NullPointerException
		NullPointerException is a built it runtime exception that is unchecked, and the TimerException is a checked exception
		that is checked at compile time.
12.  Push the updated/fixed source code to your own repository.
REPO Link: https://bitbucket.org/chasemanseth/lab8/src/master/
